---
title: About
---

I'm Andy and welcome to my little corner of the web. This is a blog where I store some thoughts, practice writing, and work on table-top games. I live in the wilds of Oregon with my family and work making software that helps the internet function.

I probably won't discuss either of those things here. 

What I will discuss is table-top games (particularly RPGs though I do enjoy a good miniatures war-game too), fiction, and personal projects.
