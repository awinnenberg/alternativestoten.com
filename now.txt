Tue Jun 21 09:56:22 AM PDT 2022

Currently working on:
  - learning tarot systems for story-telling
  - solving a problem like D&D
  - playing 7th Sea
  - playing C7's Lone Wolf
  - writing scenarios for Renegade Scout
  - teaching myself guitar

And I work a full-time job! No wonder I'm tired all the time!
